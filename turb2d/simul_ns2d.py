from math import pi
#pip install fluidsim
import fluiddyn as fld

from fluidsim.solvers.ns2d.solver import Simul

params = Simul.create_default_params()

params.output.sub_directory = "DMfluidsim" ## changer

## geometrie et maillage
params.oper.nx = params.oper.ny = nh = 64  ##128 #32
params.oper.Lx = params.oper.Ly = Lh = 2 * pi   ## L = 10
params.oper.coef_dealiasing = 0.7
delta_x = Lh / nh


params.nu_8 = 0.01 * params.forcing.forcing_rate ** (1.0 / 3) * delta_x**8 ## 1e-6


##temps de simulation
params.time_stepping.t_end = 30.0   ## 50.0

##condition intial 
params.init_fields.type = "dipole" #noise, jet

#forcage
params.forcing.enable = True
params.forcing.type = "tcrandom"  ##pseudo_spectral?,proportional
nkmax_forcing = 8.0   ## 5

params.output.periods_print.print_stdout = 0.25

params.output.periods_save.phys_fields = 1.0
params.output.periods_save.spectra = 0.5
#params.output.periods_save.spatial_means = 0.05
#params.output.periods_save.spect_energy_budg = 0.5
#params.output.periods_save.increments = 0.5

params.output.periods_plot.phys_fields = 1.0

params.output.ONLINE_PLOT_OK = True

params.output.spectra.HAS_TO_PLOT_SAVED = True
#params.output.spatial_means.HAS_TO_PLOT_SAVED = True
#params.output.spect_energy_budg.HAS_TO_PLOT_SAVED = True
params.output.increments.HAS_TO_PLOT_SAVED = True

params.output.phys_fields.field_to_plot = "rot"


sim = Simul(params)

sim.time_stepping.start()

fld.show()
