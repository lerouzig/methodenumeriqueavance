#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 28 20:01:59 2022

@author: cfd-user
"""

import os
import matplotlib.pyplot as plt
from math import pi
import fluiddyn as fld
from fluidsim.solvers.ns2d.solver import Simul
from fluidsim import load_state_phys_file

if "FLUIDSIM_TESTS_EXAMPLES" in os.environ:
    t_end = 2.0
    nh = 24
else:
    t_end = 10.0
    nh = 32
    
params = Simul.create_default_params()

params.short_name_type_run = "test1"
params.output.sub_directory = "test1"

params.oper.nx = params.oper.ny = nh = 32
params.oper.Lx = params.oper.Ly = Lh = pi

delta_x = Lh / nh

Re= 2000
U0= 0.1 # m/s
#L= 1 # m
#params.nu_8 = U0*Lh/Re
params.nu_8 = 2 * params.forcing.forcing_rate ** (1.0 / 3) * delta_x**8 
params.time_stepping.t_end = t_end

params.init_fields.type = "jet" #['from_file', 'from_simul', 'in_script', 'constant', 'noise', 'jet', 'dipole']

params.forcing.enable = True
params.forcing.type = "tcrandom_anisotropic" # "['in_script', 'in_script_coarse', 'pseudo_spectral', 'proportional', 'tcrandom', 'tcrandom_anisotropic']

params.output.periods_print.print_stdout = 0.5

params.output.periods_save.phys_fields = 1.0
params.output.periods_save.spectra = 0.5
params.output.periods_save.spatial_means = 1.0
params.output.periods_save.spect_energy_budg = 1.0
params.output.periods_save.increments = 1.0

params.output.ONLINE_PLOT_OK = True

params.output.spectra.HAS_TO_PLOT_SAVED = True #Spectre
#params.output.spatial_means.HAS_TO_PLOT_SAVED = True #NS2D E(t)
#params.output.spect_energy_budg.HAS_TO_PLOT_SAVED = True #flux d'energie PI(k_h)
#params.output.increments.HAS_TO_PLOT_SAVED = True # pdf \delta u_x(x)

params.output.phys_fields.field_to_plot = "rot"

sim = Simul(params)

#sim1 = load_state_phys_file()

sim.output.spatial_means.plot()
sim.time_stepping.start()
sim.output.phys_fields.plot()

fld.show()

